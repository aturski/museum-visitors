package com.ey.blockchain.visitors;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MuseumVisitorsTest {

    @Test
    public void mapVisitsToEvents_shouldCreateAndSortEventList() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(13, 5), LocalTime.of(13, 10)));

        //when
        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //then
        List<Pair<LocalTime, VisitEventType>> visitEvents = museumVisitors.getVisitEvents();

        assertNotNull(visitEvents);
        assertEquals(4, visitEvents.size());
        assertEquals(LocalTime.of(13, 0), visitEvents.get(0).getLeft());
        assertEquals(VisitEventType.ENTER, visitEvents.get(0).getRight());

        assertEquals(LocalTime.of(13, 5), visitEvents.get(1).getLeft());
        assertEquals(VisitEventType.ENTER, visitEvents.get(1).getRight());

        assertEquals(LocalTime.of(13, 5), visitEvents.get(2).getLeft());
        assertEquals(VisitEventType.EXIT, visitEvents.get(2).getRight());

        assertEquals(LocalTime.of(13, 10), visitEvents.get(3).getLeft());
        assertEquals(VisitEventType.EXIT, visitEvents.get(3).getRight());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnOneResultForOnePeriod() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));

        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(1, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(1), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(13, 0), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(13, 5), greatestVisitorsNumbers.get(0).getTo());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnTwoResultsForTwoDifferentPeriods() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(14, 0), LocalTime.of(14, 5)));
        visits.add(Pair.of(LocalTime.of(14, 0), LocalTime.of(14, 5)));

        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(2, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(2), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(13, 0), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(13, 5), greatestVisitorsNumbers.get(0).getTo());

        assertEquals(Integer.valueOf(2), greatestVisitorsNumbers.get(1).getVisitors());
        assertEquals(LocalTime.of(14, 0), greatestVisitorsNumbers.get(1).getFrom());
        assertEquals(LocalTime.of(14, 5), greatestVisitorsNumbers.get(1).getTo());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnOneResultsForTwoOverlappingPeriods() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(13, 2), LocalTime.of(13, 6)));

        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(1, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(2), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(13, 2), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(13, 5), greatestVisitorsNumbers.get(0).getTo());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnOneResultsWhenFirstPeriodContainsTheSecond() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(13, 2), LocalTime.of(13, 3)));

        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(1, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(2), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(13, 2), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(13, 3), greatestVisitorsNumbers.get(0).getTo());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnOneResultsWhenSecondPeriodStartEqualsFirstPeriodEnd() {
        //given
        List<Pair<LocalTime, LocalTime>> visits = new ArrayList<>();
        visits.add(Pair.of(LocalTime.of(13, 0), LocalTime.of(13, 5)));
        visits.add(Pair.of(LocalTime.of(13, 5), LocalTime.of(13, 10)));
        MuseumVisitors museumVisitors = new MuseumVisitors(visits);
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(1, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(2), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(13, 5), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(13, 5), greatestVisitorsNumbers.get(0).getTo());
    }

    @Test
    public void findGreatestVisitorsNumbers_shouldReturnTwoResultsForDataFromFile() throws IOException {
        //given
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("visiting-times.txt").getFile());
        MuseumVisitors museumVisitors = new MuseumVisitors(file.getAbsolutePath());
        //when
        List<VisitorsNumber> greatestVisitorsNumbers = museumVisitors.findGreatestVisitorsNumbers();
        //then
        assertNotNull(greatestVisitorsNumbers);
        assertEquals(1, greatestVisitorsNumbers.size());
        assertEquals(Integer.valueOf(17), greatestVisitorsNumbers.get(0).getVisitors());
        assertEquals(LocalTime.of(11, 10), greatestVisitorsNumbers.get(0).getFrom());
        assertEquals(LocalTime.of(11, 10), greatestVisitorsNumbers.get(0).getTo());
    }
}
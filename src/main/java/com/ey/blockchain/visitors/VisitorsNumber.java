package com.ey.blockchain.visitors;

import java.time.LocalTime;

/**
 * Class responsible for holding number of visitors in period of time.
 */
public class VisitorsNumber {

    private LocalTime from;
    private LocalTime to;
    private Integer visitors;

    public VisitorsNumber() {
    }

    public VisitorsNumber(LocalTime from, LocalTime to, Integer visitors) {
        this.from = from;
        this.to = to;
        this.visitors = visitors;
    }

    public LocalTime getFrom() {
        return from;
    }

    public void setFrom(LocalTime from) {
        this.from = from;
    }

    public LocalTime getTo() {
        return to;
    }

    public void setTo(LocalTime to) {
        this.to = to;
    }

    public Integer getVisitors() {
        return visitors;
    }

    public void setVisitors(Integer visitors) {
        this.visitors = visitors;
    }

    @Override
    public String toString() {
        return String.format("%s - %s; %d", from, to, visitors);
    }
}

package com.ey.blockchain.visitors;

/**
 * Type of visit event.
 */
public enum VisitEventType {
    ENTER, EXIT
}

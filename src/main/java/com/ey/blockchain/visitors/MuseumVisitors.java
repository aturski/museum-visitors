package com.ey.blockchain.visitors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class responsible for parsing visits and calculate total number of visitors in peiod of time.
 */
public class MuseumVisitors {

    private List<Pair<LocalTime, VisitEventType>> visitEvents = new ArrayList<>();
    private List<VisitorsNumber> visitorsNumbers = new ArrayList<>();

    /**
     * @param visits The list with pairs of enter and exit times. Cannot be null.
     */
    public MuseumVisitors(List<Pair<LocalTime, LocalTime>> visits) {
        mapVisitsToEvents(visits);
        processVisitEvents();
    }

    /**
     * @param pathToVisitsFile The path to file with visits.
     * @throws IOException if an I/O error occurs reading file with visits
     */
    public MuseumVisitors(String pathToVisitsFile) throws IOException {
        this(readVisitsFromFile(pathToVisitsFile));
    }

    /**
     * Method which reads file with museum visitors enter and exit times.
     * Line format: ENTER_TIME,EXIT_TIME
     *
     * @param pathToVisitsFile The path to file with visits.
     * @throws IOException if an I/O error occurs reading file with visits
     */
    public static List<Pair<LocalTime, LocalTime>> readVisitsFromFile(String pathToVisitsFile) throws IOException {
        return Files.readAllLines(Paths.get(pathToVisitsFile)).stream()
                .filter(StringUtils::isNotBlank)
                .map((line) -> {
                    String[] times = line.split(",");
                    if (times.length != 2) {
                        throw new IllegalArgumentException(String.format("Invalid time pair %s", line));
                    }
                    return Pair.of(LocalTime.parse(times[0]), LocalTime.parse(times[1]));
                })
                .collect(Collectors.toList());
    }

    /**
     * Maps a pair of event times to two events.
     *
     * @param visits The list with pairs of enter and exit times. Cannot be null.
     */
    private void mapVisitsToEvents(List<Pair<LocalTime, LocalTime>> visits) {
        for (Pair<LocalTime, LocalTime> visitorVisit : visits) {
            visitEvents.add(Pair.of(visitorVisit.getLeft(), VisitEventType.ENTER));
            // We add one minute to exit events because we want treat them as exclusive
            visitEvents.add(Pair.of(visitorVisit.getRight(), VisitEventType.EXIT));
        }

        visitEvents.sort(
                Comparator.comparing((Function<Pair<LocalTime, VisitEventType>, LocalTime>) Pair::getLeft)
                        .thenComparing(Pair::getRight));
    }

    /**
     * Processes visit events and calculates visitors number.
     */
    private void processVisitEvents() {
        int visitors = 0;
        Pair<LocalTime, VisitEventType> previousVisitEvent = null;
        for (Pair<LocalTime, VisitEventType> visitEvent : visitEvents) {
            if (previousVisitEvent != null) {
                if (!previousVisitEvent.getLeft().equals(visitEvent.getLeft()) ||
                        !previousVisitEvent.getRight().equals(visitEvent.getRight())) {
                    visitorsNumbers.add(new VisitorsNumber(previousVisitEvent.getLeft(), visitEvent.getLeft(), visitors));
                }
            }

            if (visitEvent.getRight() == VisitEventType.ENTER) {
                visitors += 1;
            } else {
                visitors -= 1;
            }

            previousVisitEvent = visitEvent;
        }
    }

    /**
     * @return The Optional with MuseumVisitorsNumber object which has the greatest visitors number.
     */
    public List<VisitorsNumber> findGreatestVisitorsNumbers() {
        return visitorsNumbers.stream()
                .collect(Collectors.groupingBy(VisitorsNumber::getVisitors, Collectors.toList()))
                .entrySet().stream()
                .max(Map.Entry.comparingByKey())
                .get()
                .getValue();
    }


    /**
     * @return The visitors numbers list
     */
    public List<VisitorsNumber> getVisitorsNumbers() {
        return visitorsNumbers;
    }


    /**
     * @return The visit events list
     */
    public List<Pair<LocalTime, VisitEventType>> getVisitEvents() {
        return visitEvents;
    }

    /**
     * @param args Program arguments. The first argument in array has to be a path to file with visits file.
     * @throws IOException Exception thrown
     */
    public static void main(String[] args) throws IOException {
        if (args == null || args.length < 1 || args[0] == null) {
            throw new IllegalArgumentException("Missing argument with path to visits file.");
        }

        MuseumVisitors museumVisitors = new MuseumVisitors(args[0]);
        museumVisitors.getVisitorsNumbers().forEach(System.out::println);

        System.out.println("-------------------");
        System.out.println("The greatest visitors number were between: ");
        museumVisitors.findGreatestVisitorsNumbers().forEach(System.out::println);
    }

}

# Finding the time period algorithm
1. Read file with visits: for every visit create a pair of a start and end of a visit.
2. Break pairs into two events ENTER and EXIT add them to an event list. For EXIT events add 1 minute to event date because they are inclusive and we want them to be exclusive dates.    
3. Sort event list ascending by event date.
4. Process sorted event lists:
    - for every ENTER event increment visitors,
    - for every EXIT event decrement visitors, 
    - for every event (except first) create object which holds period (from, to) and visitors number
5. Find max by visitors on a list of object with visitors number - can be many results.